// package main

// import "fmt"

// // chan<- escritura 
// func Generator(c chan<- int) {
// 	for i := 1; i <= 10; i++ {
// 		c <- 1
// 	}
// 	close(c)
// }
// // lectura es <-chan
// func Double(in <-chan int, out chan<- int) {
// 	for value := range in {
// 		out <- 2 * value
// 	}
// 	close(out)
// }

// func Print(c chan int) {
// 	for value := range c {
// 		fmt.Println(value)
// 	}
// }

// func main() {
// 	generator := make(chan int)
// 	doubles := make(chan int)

// 	go Generator(generator)
// 	go Double(generator, doubles)
// 	Print(doubles)
// }