package main

import "fmt"

// esta es la forma de hacer herencia y composicion
type Person struct {
	name string
	age int
}

type BaseEmployee struct {
	id int
}

type FullTimeEmployee struct {
	// campo anonimo
	Person
	BaseEmployee
	endDate string
}

func (ftEm FullTimeEmployee) getmessage() string {
	return "Full Time Employee"
}

type TemporaryEmployee struct {
	Person
	BaseEmployee
	taxRate int
}

func (tEm TemporaryEmployee) getmessage() string {
	return "Temporary Employee"
}

type PrintInfo interface {
	getmessage() string
}

func getmessage(p PrintInfo) {
	fmt.Println(p.getmessage())
}

func main() {
	ftEmployee := FullTimeEmployee{}
	ftEmployee.name = " Name"
	ftEmployee.age = 28
	ftEmployee.id = 1
	fmt.Println(ftEmployee)
	tEm := TemporaryEmployee{}
	getmessage(tEm)
	getmessage(ftEmployee)

}