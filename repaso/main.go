package main

import (
	"fmt"
	"strconv"
)

func main() {
	var x int = 8
	y := 7
	fmt.Println(x + y)
	myValue, err := strconv.ParseInt("7", 0, 64)
	if err != nil {
		fmt.Printf("%v\n", err)
	} else {
		fmt.Println(myValue)
	}

	// maps estructura de llave valor
	m := make(map[string]int)
	m["key"] = 6
	fmt.Println(m["key"])

	// slice es parecido a un array
	s := []int{1,2,3}
	for index, value := range s {
		fmt.Println(index)
		fmt.Println(value)
	}
	// para agregar un valor nuevo en el slice s
	s = append(s, 18)
	for index, value := range s {
		fmt.Println(index)
		fmt.Println(value)
	}
}