package main

import "github.com/donvito/hellomod"

// En este ejemplo se utiliza modulos o paquetes de otros
// para descargar se debe ingresar el comando go get y el nombre del repositorio de paquete ejemplo go get github.com/donvito/hellomod
// En caso de no utilizarlo pero haberlo descargado existe un comando para borrar los paquetes que no se usan y es go mod tidy
// Para visualizar la ubicación de los paquetes se realiza con el comanod go mod download -json

func main() {
	hellomod.SayHello()
}
