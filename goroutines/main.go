package main

import (
	"fmt"
	"time"
)

func main() {
	var x, y int
	x = 5
	y = 3
	fmt.Println("la suma de los dos valores es", x + y)
	// asi se crea un canal
	c := make(chan int)
	go doSomething(c)
	<- c

	// apuntadores
	g := 25
	fmt.Println(g)
	h := &g
	fmt.Println(h)
	// para acceder al valor y no al lugar en memoria que esta guardado usamos
	fmt.Println(*h)
}

func doSomething(c chan int) {
	time.Sleep(3 * time.Second)
	fmt.Println("It is done")
	c <- 1
}